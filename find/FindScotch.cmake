
SET(SCOTCH_FOUND False)

FIND_PATH(SCOTCH_INCLUDES NAMES scotch.h
                          PATHS /usr/include /usr/local/Cellar/
                          PATH_SUFFIXES scotch)

FIND_LIBRARY(SCOTCH_LIBRARIES scotch 
             PATHS /usr/lib)

IF(SCOTCH_INCLUDES AND SCOTCH_LIBRARIES)
  SET(SCOTCH_FOUND True)
ENDIF(SCOTCH_INCLUDES AND SCOTCH_LIBRARIES)

